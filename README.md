# JSON Multiplier #

Der JSON Multiplier ist ein kleines Tool, mit dem sich ein einfaches JSON Objekt zu einem beliebig großen JSON-Array multiplizieren lässt.  
Wichtig ist, dass die jar-Datei **jsonMultiplier.jar** im selben Verzeichnis wie die Dateien **jsonMultiplier.txt** und **jsonMultiplierOutput.txt**  
liegt.  
In die jsonMultiplier.txt muss das zu duplizerende JSON-Objekt eingefügt werden. Das generierte JSON-Array wird dann in die Datei  
jsonMultiplierOutput.txt geschrieben.

Die ausführbare jar-Datei und die beiden *.txt Dateien befinden sich unter Downloads (ganz unten). 

### jsonMultiplier.txt ###
Beispiel für jsonMultiplier.txt (WICHTIG! Es dürfen keine eckigen Klammern mit eingegeben werden!)
```
{
"Feld1":"Wert1",
"Feld2":"Wert2",
"Feld3":"Wert3",
"Feld4":"Wert4"
}
```

### jsonMultiplierOutput.txt ###
Wenn das Tool erfogreich gelaufen ist, dann befindet sich das generierte json_Array in der Datei jsonMultiplierOutput.txt.  

### Hilfe ausgeben lassen ###
```
java -jar jsonMultiplier.jar help
```

### Tool starten ###
```
java -jar jsonMultiplier.jar <anzahl>
```
Beispiel für ein json-Array mit 5000 json-Objekten:
```
java -jar jsonMultiplier.jar 5000
```  
  
### Tool starten mit generiertem Feld ID ###
Wenn zusätzlich zur Anzahl auch noch der Parameter "-id" mit angegeben wird, dann wird das JSON um ein Feld ID erweitert  
und eine Id inkrementell hochgezählt.
```
java -jar jsonMultiplier.jar <anzahl> -id
```

### Download jsonMultiplier ###
(https://bitbucket.org/svebeh/jsonmultiplier/downloads/)