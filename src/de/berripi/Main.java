package de.berripi;

import java.io.IOException;
import java.util.List;

public class Main {

    //TODO ID inkrementell hochzaehlen einbauen
    //TODO Versionierung implementieren

    public static void main(String[] args) throws IOException {
/*
{
"Name":"Behrens",
"Vorname":"Sven",
"Stadt":"Hamburg"
}
* */
        boolean incrementId = false;

        if(args.length == 0){
            System.out.println("Bitte die Anzahl der JSON Objekte als Parameter uebergeben! " +
                    "\nBeispiel: java -jar jsonMultiplier.jar 22" +
                    "\nBeispiel: java -jar jsonMultiplier.jar 22 -id (wird -id angegeben, dann wird ein Feld id hinzugefuegt und inkrementell hochgezaehlt.)" +
                    "\nBeispiel: java -jar jsonMultiplier.jar -help");
            System.exit(0);
        }
        if(args[0].equals("-help")){
            String printHelp = Help.help();
            System.out.println(printHelp);
            System.exit(0);
        }

        if(args.length == 2){
            if (args[1].equals("-id")) {
                incrementId = true;
            }
        }

        int anzahl = Integer.parseInt(args[0]);
        List<String> jsonListe;
        jsonListe = ReadFile.readFile("jsonMultiplier.txt", anzahl, incrementId);
        String rc = CreateFile.createFile(jsonListe);
        System.out.println(rc);
    }
}
