package de.berripi;

import java.io.*;
import java.util.List;

public class CreateFile {

    public static String createFile(List<String> jsonListe){
        PrintWriter pWriter = null;

        try {
            pWriter = new PrintWriter(new FileOutputStream("jsonMultiplierOutput.txt"));
            for (Object json : jsonListe) {
                pWriter.print(json + "\n");
            }
            return "JSON wurde in jsonMultiplierOutput.txt generiert.";
        } catch (IOException e) {
            e.printStackTrace();
            return "Es ist ein Fehler aufgetreten. Das JSON wurde nicht generiert!";
        } finally {
            if (pWriter != null) {
                pWriter.flush();
                pWriter.close();
                //return "JSON wurde in jsonMultiplierOutput.txt generiert.";
            }
        }
    }
}
