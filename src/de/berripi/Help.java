package de.berripi;

public class Help {

    public static String help(){

        return "Der jsonMultiplier nimmt ein einfaches JSON-Objekt entgegen und dupliziert es um den Faktor n (Uebergabeparameter).\n" +
                "Das zu duplizierende JSON-Objekt muss in die Datei jsonMultiplier.txt eingefuegt werden. Die Datei muss im selben Verzeichnis wie \n" +
                "das Programm jsonMultiplier.jar liegen. Das Programm wird mit \"java -jar jsonMultiplier.jar <anzahl>\" gestartet.\n" +
                "Das generierte JSON befindet sich dann in der Datei jsonMultiplierOutput.txt. \n" +
                "Wenn der Parameter -id uebergeben wird, dann wird das Feld \"id\" hinzugefuegt und inkrementell hochgezaehlt, oder ein \n" +
                "vorhandenes ID-Feld inkrementell hochgezaehlt.";
    }
}
