package de.berripi;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReadFile {

    public static List<String> readFile(String file, int anzahl, boolean incrementId) throws IOException {

        BufferedReader bf = new BufferedReader(new FileReader(file));
        List<String> newJson = new ArrayList<>();
        int idCounter = 1;
        int counter = 0;
        String line;

        //JSON einlesen und in ArrayList einfuegen.
        while((line = bf.readLine()) != null){
            counter++;
            //Wenn als Uebergabeparameter incrementId true uebergeben wurde, dann wird eine ID erzeugt und hochgezaehlt.
            if(line.toLowerCase().contains("id") && incrementId){
               line = line.replaceAll("[idID\"]+:[0-9a-zA-z\",]+","\"id\":\""+idCounter+"\"");
               idCounter++;
            }
            newJson.add(line);
        }
        // Komma nach geschlossener geschweifter Klammer setzen um das JSON zu erweitern.
        newJson.set(counter-1, "},");

        // Das vorhandene JSON um den Faktor n (anzahl) duplizieren.
        for(int i=0; i<counter*(anzahl-1); i++){
            //Wenn als Uebergabeparameter incrementId true uebergeben wurde, dann wird eine ID erzeugt und hochgezaehlt.
            if(newJson.get(i).toLowerCase().contains("id") && incrementId){
                newJson.add(newJson.get(i).replaceAll("[idID\"]+:[0-9a-zA-z\",]+","\"id\":\""+idCounter+"\""));
                idCounter++;
            } else {
                newJson.add(newJson.get(i));
            }
        }

        //Aus dem JSON ein JSON-Array machen indem die eckige Klammer an den Anfang und an das Ende des JSONs gesetzt wird.
        newJson.set(0, "[\n"+newJson.get(0));
        newJson.add("]");

        //Das letzte Komma hinter der letzten geschweiften Klammer entfernen.
        newJson.set(counter*(anzahl)-1, "}");

        for (String x : newJson
        ) {
            System.out.println(x);
        }

        return newJson;
    }
}
